const RoomRepository = require('../db/repositories/RoomRepository.js');
const { getFree } = require('../db/repositories/RoomRepository.js');


module.exports = {
    async createRoom(roomNumber) {
        try {
            const room = new RoomRepository(roomNumber);
            const result = await room.save();
            return result;
        } catch(err) {
            console.log('roomServises -> createRoom error :::', err.message);
            throw err;
        }
    },
    async getAll() {
        try {
            const result = await RoomRepository.getAll();
            return result;
        } catch(err) {
            console.log('roomServises -> getAll error :::', err.message);
            throw err;
        }
    },

    async getFree(startDate, finishDate) {
        try {
            const result = await RoomRepository.getFree(startDate, finishDate);
            return result;
        } catch(err) {
            console.log('roomServises -> getFree error :::', err.message);
            throw err;
        }
    }
}