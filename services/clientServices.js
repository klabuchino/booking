const ClientRepository = require('../db/repositories/ClientRepository.js');
const helpers = require('../helpers.js');
const jwt = require('jsonwebtoken');
require('dotenv').config();
const secret = process.env.JWT_SECRET;

module.exports = {
    async createClient(name, surname, email, password, isVIP) {
        try {
            const client = new ClientRepository(name, surname, email, password, isVIP);
            const result = await client.save();
            return result;
        } catch (err) {
            console.log('clientServices->createClient error ::: ', err.message );
            throw err;
        }
    },

    async authenticate(email, password, callback) {
        try {
            const client = await ClientRepository.findOne(email);
            console.log(client);
            if( !client ) {
                callback( { message:'Неправильная почта или пароль' } );
            }
            else {
                ClientRepository.isCorrectPassword(client.password, password, function(err, same) {
                    if( err ) {
                        callback( { message: 'что-то пошло не так...' } );
                    }
                    else if( !same ) {
                        callback( { message: 'неправильная почта или пароль' } );
                    }
                    else {
                        const payload = { 
                            client: {
                                id: client.id,
                                isVIP: client.isvip
                            }
                        };
                        const token = jwt.sign( payload, secret, {
                        expiresIn: '7d'
                        });
                        callback(null, token);
                    }
                });
            }
        }catch (err) {
            callback(err);
        }
    }
}