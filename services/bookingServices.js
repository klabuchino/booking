const BookingRepository = require('../db/repositories/BookingRepository.js');


module.exports = {
    async createBooking(roomId, clientId, isVIP, startDate, finishDate) {
        try {
            const booking = new BookingRepository(roomId, clientId, isVIP, startDate, finishDate);
            const isBooked = await booking.isBooked();
            if( isBooked ) {
                return { message: 'на данный период забронировать нельзя' };
            }
            const result = await booking.save();
            return result;
        } catch(err) {
            console.log('bookingServices->createBooking errror :::', err.message);
            throw err;
        }
    },

    async delete(id, clientId) {
        try {
            const result = await BookingRepository.delete(id, clientId);
            return result;
        } catch (error) {
            console.log('bookingServices->delete errror :::', err.message);
            throw err;
        }
    }
}