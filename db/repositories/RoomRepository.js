const db = require('../query.js');


class RoomRepository {
    constructor(roomNumber) {
        this.roomNumber = roomNumber;
    }

    static async getAll() {
        const text = `SELECT * FROM room`;
        try {
            const result = await db.query(text);
            return result.rows;
        } catch(err) {
            console.log('RoomRepository->getAll error ::: ', err.message);
            throw(err);
        }
    }

    static async getFree(startDate, finishDate) {
        const text = `SELECT room.id, room_number 
        FROM room 
        WHERE room.id NOT IN 
        (SELECT room_id FROM booking 
        WHERE $1 <= finish_date AND $2 >= start_date)`;
        const values = [ startDate, finishDate ];

        try {
            const result = await db.query(text, values);
            return result.rows;
        } catch(err) {
            console.log('RoomRepository->getFree error::: ', err.message);
            throw(err);
        }
    }
}

module.exports = RoomRepository;