const db = require('../query.js');

class BookingRepository {
    constructor(roomId, clientId, isVIP, startDate, finishDate) {
        this.roomId = roomId;
        this.clientId = clientId;
        this.isVIP = isVIP;
        this.startDate = startDate;
        this.finishDate = finishDate;
    }

    async isBooked() {
        const text = `SELECT * FROM booking
        WHERE room_id = $1 AND ($2 <= finish_date AND $3 >= start_date)`;
        const values = [ this.roomId, this.startDate, this.finishDate ];
        try {
            const result = await db.query(text, values);
            if(result.rows.length === 0) {
                return false;
            }
            return true;
        } catch(err) {
            console.log( 'BookingRepository->isBooked error ::: ', err.message);
            throw(err);
        }
    }

    async save() {
        const text = `INSERT INTO booking
        (room_id, client_id, isVIP, start_date, finish_date)
        VALUES($1, $2, $3, $4, $5)
        RETURNING *`;
        const values = [ this.roomId, this.clientId, this.isVIP, this.startDate, this.finishDate ];
        try {
            const result = await db.query(text, values);
            return result.rows[0];
        } catch(err) {
            console.log( 'BookingRepository->save error ::: ', err.message);
            throw(err);
        }
    }

    static async delete(id, clientId) {
        const text = `DELETE FROM booking WHERE id = $1 AND client_id = $2`;
        const values = [ +id, +clientId];
        try {
            const result = await db.query(text, values);
            return { message: 'бронирование успешно удалено' };
        } catch (err) {
            console.log( 'BookingRepository->delete error ::: ', err.message);
            throw(err);
        }
    }
}

module.exports = BookingRepository;