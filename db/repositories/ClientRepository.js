const db = require('../query.js');
const bcrypt = require('bcrypt');

const saltRounds = 10;

class ClientRepository {
    constructor(name, surname, email, password, isVIP) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.isVIP = isVIP;
    }

    async save() {
        const text = `INSERT INTO client
        (name, surname, email, password, isvip)
        VALUES($1, $2, $3, $4, $5)
        RETURNING *`;
        
        try {
            const hashedPassword = await bcrypt.hash(this.password, saltRounds);
            const values = [ this.name, this.surname, this.email, hashedPassword, this.isVIP ];
            const result = await db.query(text, values);
            return result.rows[0];
        } catch(err) {
            console.log( 'ClientRepository->save error ::: ', err.message );
            throw(err);
        }
    }

    static async findOne(email) {
        const text = `SELECT * FROM client WHERE email = $1`;
        const values = [ email ];
        try {
            const result = await db.query(text, values);
            const client = result.rows[0];
            if(client) {
                return client;
            } 
            return;
        } catch (err) {
            console.log( 'ClientRepository->findOne error ::: ', err.message );
            throw(err);
        }
    }

    static async isCorrectPassword(password, inputPassword, callback) {
        bcrypt.compare(inputPassword, password, function(err, same) {
            if(err) {
                callback(err);
            }
            else {
                callback(err, same);
            }
        })
    }
}

module.exports = ClientRepository;