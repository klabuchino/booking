const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

//const indexRouter = require('./routes/index');
const clientsRouter = require('./routes/clients');
const roomsRouter = require('./routes/rooms');
const bookingRoter = require('./routes/booking');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//app.use('/', indexRouter);
app.use('/clients', clientsRouter);
app.use('/rooms', roomsRouter);
app.use('/booking', bookingRoter);

module.exports = app;
