const jwt = require('jsonwebtoken');
require('dotenv').config();
const secret = process.env.JWT_SECRET;

const withAuth = function(req, res, next) {
    const token = req.cookies.token;

    if(!token) {
        console.log('нет токена')
        res.json( { error: 'не авторизован' } );
    }
    else {
        jwt.verify(token, secret, function(err, decoded) {
            if(err) {
                console.log('некорректный токен', err)
                res.json( { error: 'не авторизован' } );
            }
            else {
                req.client = decoded.client;
                next();
            }
        })
    }
};
module.exports = withAuth;