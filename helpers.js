module.exports = {
    isCorrectEmail(email) {
        const reg = /\S+@\S+\.\S+/;
        return reg.test(email);
    },
    isNotEmpty (arrParams) {
        const index = arrParams.findIndex(elem => !!elem === false);
        if(index !== -1) {
            return false;
        }
        return true;
    },

    //приведение даты к виду ГГГГ-ММ-ДД
    createCorrectDate(date) {
        try {
            const newDate = new Date(date);
            const year = newDate.getFullYear().toString();
            let mounth = (newDate.getMonth() + 1).toString();

            //добавление нуля в начало если число из одного знака
            mounth = mounth.length === 1 ? `0${mounth}` : mounth;

            let day = newDate.getDate().toString();
            day = day.length === 1 ? `0${day}` : day;

            return `${year}-${mounth}-${day}`;
        } catch (err) {
            console.log('helpers->createCorrectDate error :::', err.message);
            throw err;
        }
    },

    //проверка правильности начальной и конечной дат (что они идут в правильном порядке)
    isInOrderDates(start, finish) {
        try {
            if( new Date(start) > new Date(finish) ) {
                return false;
            }
            return true;
        } catch (err) {
            console.log('helpers->isInOrderDates error :::', err.message);
            throw err;
        }
    }
}