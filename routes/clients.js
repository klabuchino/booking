const express = require('express');
const router = express.Router();
const clientServices = require('../services/clientServices');
const helpers = require('../helpers');
const withAuth = require('../midllewares/withAuth');

router.post('/registry', async (req, res, next) => {
  try {
    const { name, surname, email, password, isVIP } = req.body;
    const newEmail = email.trim();

    const isCorrectEmail = helpers.isCorrectEmail(newEmail);
    const isNotEmpty = helpers.isNotEmpty([ name, surname, email, password ]);
    
    if(isCorrectEmail && isNotEmpty) {
      const result = await clientServices.createClient(name, surname, newEmail, password, isVIP);
      delete result.password;
      res.json(result);
    }
    else if(!isCorrectEmail) {
      res.status(400).json({ message: 'неправильный email' });
    }
    else {
      res.status(400).json( {message: 'отсутствуют данные'} );
    }
    
  } catch(err) {
    res.status(500).json({ message: 'внутренняя ошибка сервера', error: err.message });
  }
})

router.post('/authenticate', async (req, res, next) => {
  const { email, password } = req.body;
    clientServices.authenticate(email, password, (err, token) => {
      if(err) {
        res.status(400).json({ message:  err.message });
      }
      else {
        res.cookie('token', token, { httpOnly: true } ).sendStatus(200);
      }
    });
})

router.get('/checkAuth', withAuth, (req, res, next) => {
  res.json(req.client);
})

module.exports = router;
