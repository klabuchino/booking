const express = require('express');
const router = express.Router();
const roomServises = require('../services/roomServices.js');
const helpers = require('../helpers.js');

router.get('/get_all', async (req, res, next) => {
    try {
        const result = await roomServises.getAll();
        res.json(result);
    } catch(err) {
        res.status(500).json({ message: 'внутренняя ошибка сервера', error: err.message });
    }
});

router.get('/get_free', async (req, res, next) => {
  try {
    const { startDate, finishDate } = req.query;
    const isInOrderDates = helpers.isInOrderDates(startDate, finishDate);
    const newStartDate = helpers.createCorrectDate(startDate);
    const newFinishDate = helpers.createCorrectDate(finishDate);

    if(isInOrderDates) {
      const result = await roomServises.getFree(newStartDate, newFinishDate);
      res.json(result);
    }
    else {
      res.status(400).json({ message: 'начальная дата позже конечной даты' });
    }
  } catch(err) {
    res.status(500).json({ message: 'внутренняя ошибка сервера', error: err.message });
  }
})

module.exports = router;
