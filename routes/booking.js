const express = require('express');
const router = express.Router();
const bookingServices = require('../services/bookingServices.js');
const helpers = require('../helpers.js');
const withAuth = require('../midllewares/withAuth');


router.post('/create', withAuth, async (req, res, next) => {
    try {
        console.log(req.client)
        const { roomId, startDate, finishDate } = req.body;

        const isNotEmpty = helpers.isNotEmpty([ roomId, startDate, finishDate ]);
        const isInOrderDates = helpers.isInOrderDates(startDate, finishDate);
        const newStartDate = helpers.createCorrectDate(startDate);
        const newFinishDate = helpers.createCorrectDate(finishDate);

        if(isNotEmpty && isInOrderDates) {
            const booking = await bookingServices.createBooking(roomId, req.client.id, req.client.isVIP, newStartDate, newFinishDate);
            res.json(booking)
        }
        else if(!isInOrderDates) {
            res.status(400).json({ message: 'начальная дата позже конечной даты' });
        }
        else {
            res.status(400).json({ message: 'отсутствуют данные' });
        }
    } catch(err) {
        res.status(500).json({ message: 'внутренняя ошибка сервера', error: err.message });
    }
})

router.delete('/delete', withAuth, async (req, res, next) => {
    try {
        if(!req.body.id) {
            res.status(400).json({ message: 'отсутсвует поле id' });
        }
        else {
            const result = await bookingServices.delete(req.body.id, req.client.id);
            res.json(result);
        } 
    } catch (err) {
            res.status(500).json({ message: 'внутренняя ошибка сервера', error: err.message });
    }
})


module.exports = router;
